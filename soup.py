import re
import sys
import threading

class Node:
    def __init__(self, char, j, k):
        self.value = char
        self.location = (j,k)
        self.neighborsSet = False
        self.nearestNeighbors = {}    
    
    def getValue(self):
        return self.value

    def getLocation(self): 
        return self.location

    def getNeighborsSet(self):
        reutrn self.neighborsSet

    def getNeighbors(self, out):
        return self.nearestNeighbors

    def printNeighbors(self)
        for nodeValue, nodeLocation in self.nearestNeighbors():
            print(nodeValue + ": " + nodeLocation)

    def setNeighbor(board, j, k):
        node = board[j-1][k]
        self.nearestNeighbors[node.getValue()] = node.getLocation() 
 
    def addNeighors(board): 
        cols=board.getColumns()
        rows=board.getRows()
        j, k = getLocation() 
        if j > 0: 
            setNeighbor(board, j-1, k) 
            if k > 0: 
                setNeighbor(board, j-1, k-1) 
            if k < cols-1: 
                setNeighbor(board, j-1, k+1)
        if j < rows-1: 
            setNeighbor(board, j+1, k)
            if k > 0:
                setNeighbor(board, j+1, k-1)
            if k < cols-1:
                setNeighbor(board, j+1, k+1)   
        if k > 0: 
            setNeighbor(board, j, k-1)
        if k < cols-1:
            setNeighbor(board, j, k+1)
        
        self.neighborsSet = True

class Board:
    def __init__(self, rows, columns):
        self.rows = rows
        self.columns = columns
        self.board = [[0 for _ in range(cols)] for _ in range(rows)]

    def getRows(self):
        return self.rows
    
    def getColumns(self):
        return self.columns

    def getNode(self, j, k):
        return self.board[j][k]

    def build(self, j, data):
        for k in range(self.columns):
            self.board[j][k] = Node(str(data[k]), j, k)

    def getBoard(board):
        for j in range(self.rows):
            for k in range(self.columns):
                print(board[j][k].value + " ", end='')
        print()

def determineVector(n1, n2):
    j1, k1 = n1.getLocation()
    j2, k2 = n2.getLocation()
    j = j1 - j2
    k = k1 - k2
    return (j, k) 

def main():
    if len(sys.argv) !=2:
        print("Usage: python3 soup.py <filename>")
        return
    
    filename = sys.argv[1]
    words = ()
    try:
        i = 0
        with open(filename, 'r') as soup:
            for line in soup: 
                if i == 0:
                    dimensions = line.split("x")
                    b = board(int(dimensions[0]), int(dimensions[1]))
                elif i > 0 and i <= rows:
                    b = board.build(j-1,line.strip("\n").split())
                else: 
                    words.append(line)
                         
        i+=1 
        for word in words: 
            for j in range(b.getRows()):
                for k in range(b.getColumns()):
                    n = board.getNode(j,k)
                    if !n.getNeighborsSet(): 
                        n.addNeighbors(board)
                        n.printNeighbors()
                    nodeValue = n.getValue()
                    match = re.search(nodeValue, word)
                    if match: 
                       begin 
        
        for j in range(b.getRows()):
            for k in range(b.getColumns()):
                         
    except FileNotFoundError: 
        print("Error: File '{filename}' not found.")
    
    #getBoard(board)            
if __name__ == "__main__":
    main()
